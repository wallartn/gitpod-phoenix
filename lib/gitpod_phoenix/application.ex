defmodule GitpodPhoenix.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      GitpodPhoenixWeb.Telemetry,
      # Start the Ecto repository
      GitpodPhoenix.Repo,
      # Start the PubSub system
      {Phoenix.PubSub, name: GitpodPhoenix.PubSub},
      # Start Finch
      {Finch, name: GitpodPhoenix.Finch},
      # Start the Endpoint (http/https)
      GitpodPhoenixWeb.Endpoint
      # Start a worker by calling: GitpodPhoenix.Worker.start_link(arg)
      # {GitpodPhoenix.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: GitpodPhoenix.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    GitpodPhoenixWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
