defmodule GitpodPhoenix.Repo do
  use Ecto.Repo,
    otp_app: :gitpod_phoenix,
    adapter: Ecto.Adapters.Postgres
end
