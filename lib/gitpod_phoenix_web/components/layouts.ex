defmodule GitpodPhoenixWeb.Layouts do
  use GitpodPhoenixWeb, :html

  embed_templates "layouts/*"
end
