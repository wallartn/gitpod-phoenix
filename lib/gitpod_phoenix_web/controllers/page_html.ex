defmodule GitpodPhoenixWeb.PageHTML do
  use GitpodPhoenixWeb, :html

  embed_templates "page_html/*"
end
